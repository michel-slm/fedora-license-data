# This file is for collecting the actual text found in Fedora
# packages that have used the Callaway short name, "Freely redistributable without restriction" 
# or other texts that are comprised of ultra-permissive licenses that contain 
# no conditions upon the license grant, regardless of what Callaway 
# short name may have been used. The License: field for packages that have text listed here can use 
# the SPDX-conformant identifier, "LicenseRef-Fedora-UltraPermissive" going forward.
# For more information, see the instructions at https://docs.fedoraproject.org/en-US/legal/update-existing-packages/#_callaway_short_name_categories
#
# Include the following information:
# 
# Fedora package name
#
# Location of where you found the license notice text.
# Preferably this would be a direct link to a file. If that is not possible,
# provide enough information such that someone else can find the text in the wild
# where you found it.
#
# The actual text found that corresponds to the use of 
# the "Freely redistributable without restrictions" (previously) or (proposed use of) "LicenseRef-Fedora-MaxPermissive" identifiers.
#
# Copy template below and add yours to top of list, adding a space between entries.
package = 
location = 
text = '''
text here
'''

package = netpbm
location = https://sourceforge.net/p/netpbm/code/HEAD/tree/stable/converter/ppm/ppmtoacad.c#l15
text = '''
Permission to use, copy, modify, and distribute this software and its documentation for any purpose and without fee is hereby granted, without any conditions or restrictions. This software is provided “as is” without express or implied warranty.
'''

package = man-pages
location = https://github.com/mkerrisk/man-pages/blob/master/man2/futex.2
text = '''
may be freely modified and distributed
'''

package = man-pages
location = https://github.com/mkerrisk/man-pages/blob/master/man2/getitimer.2
text = '''
May be freely distributed and modified
'''

package = man-pages
location = https://github.com/mkerrisk/man-pages/blob/master/man3/getpt.3
text = '''
Redistribute and modify at will.
'''

package = man-pages
location = https://github.com/mkerrisk/man-pages/blob/master/man4/pts.4
text = '''
Redistribute and revise at will.
'''

package = perl
location = https://github.com/Perl/perl5/blob/blead/pod/perlunicook.pod#copyright-and-licence
text = '''
Most of these examples taken from the current edition of the “Camel Book”;
that is, from the 4ᵗʰ Edition of I<Programming Perl>, Copyright © 2012 Tom
Christiansen <et al.>, 2012-02-13 by O’Reilly Media.  The code itself is
freely redistributable, and you are encouraged to transplant, fold,
spindle, and mutilate any of the examples in this manpage however you please
for inclusion into your own programs without any encumbrance whatsoever.
Acknowledgement via code comment is polite but not required.
'''

package = perl-XML-Writer
location = https://metacpan.org/release/JOSEPHW/XML-Writer-0.900/source/LICENSE
text '''
Writer.pm - write an XML document.

Copyright (c) 1999 by Megginson Technologies.
Copyright (c) 2003 Ed Avis <ed@membled.com>
Copyright (c) 2004-2010 Joseph Walton <joe@kafsemo.org>

Redistribution and use in source and compiled forms, with or without
modification, are permitted under any circumstances.  No warranty.
'''

package = texlive-kix
location = https://mirrors.rit.edu/CTAN/fonts/kixfont/kix.mf
text = '''
Available for any purpose, no warranties.
'''

package = texlive-docbytex
location = https://mirror.math.princeton.edu/pub/CTAN/macros/generic/docbytex/README
text = '''
You can do anything with the files from DocBy.TeX package without 
any limit. If the macro will be usable for you, you can tell the author 
about it. There is no warranty for this macro.
'''

package = texlive-courseoutline
location = https://ctan.math.utah.edu/ctan/tex-archive/macros/latex/contrib/courseoutline/courseoutline.cls
text = '''
Feel free to copy, modify, and distribute.
I am interested in all changes you make.
Please send changes to ngall@ucalgary.ca
'''

package = hanamin-fonts
location = http://fonts.jp/hanazono/
text = '''
This font is a free software.
Unlimited permission is granted to use, copy, and distribute it, with
or without modification, either commercially and noncommercially.
THIS FONT IS PROVIDED "AS IS" WITHOUT WARRANTY.
License of this document is same as the font.

Copyright 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2016, 2017 GlyphWiki Project.
'''

package = ibus-table-chinese
location = https://github.com/mike-fabian/ibus-table-chinese
text = '''
Freely redistributable without restriction
'''
# Also occurs in one file as "This table is freely redistributable without restriction"
# Should not be confused with the identical Callaway metadata name
# For extensive discussion see:
# https://gitlab.com/fedora/legal/fedora-license-data/-/issues/6
# https://gitlab.com/fedora/legal/fedora-license-data/-/issues/106
# (which approves use of LicenseRef-Fedora-MaxPermissive for this case)
# https://gitlab.gnome.org/GNOME/gnome-software/-/issues/2004
